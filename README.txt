- Intro

It was used clean architecture design to implement this test. Here is the explanation if you never heard about: https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-
architecture.html

Also the repository tests are using a H2 embedded database(not the same that the application use) for better tests :)

- Extra libs
Lombok to reduce the code with getters and setters. (Its necessary to install a plugin on intellij to stop the compilation errors) 

Lombok plugin link: https://plugins.jetbrains.com/plugin/6317-lombok-plugin

- How to compile: mvn clean install
- How to run testes: mvn test
- How to run the application: mvn spring-boot:run

- URL example for each call

1) a) Create Product
POST /products
Body : 
{
  "name" : "name",
  "description" : "description",
  "parentProductId: 1
}

1) b) Update Product
PUT /products/{id}
Body : 
{
  "name" : "name",
  "description" : "description",
  "parentProductId: 1
}

1) c) Delete Product
DELETE /products/{id}

2) a) Create Image
POST /images
Body : 
{
  "type" : "type",
  "productId: 1
}

2) b) Update Image
PUT /images/{id}
{
  "type" : "type",
  "productId: 1
}

2) c) Delete Image
DELETE /images/{id}

3) Get all products excluding relationships (child products, images)
GET /products?loadChildren=false&loadImage=false
 
4) Get all products including specified relationships (child product and/or images)
GET /products?loadChildren=true&loadImages=true

5) Same as 3 using specific product identity
GET /products?loadChildren=false&loadImages=false

6) Same as 4 using specific product identity 
GET /products?loadChildren=true&loadImages=true

7) Get set of child products for specific product 
GET /products/children?parentProductId={id}

8) Get set of images for specific product
GET /images?productId={id}


- Improvements

Implement endpoint tests and integration tests with Cucumber to upgrade the tests pyramid acording the uncle bob test automation pyramid. (The time was short to complete the test)

Implement a Custom Exception Handler. (I tried many solutions but no success)

