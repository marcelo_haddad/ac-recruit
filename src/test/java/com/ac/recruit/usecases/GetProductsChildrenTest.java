package com.ac.recruit.usecases;

import com.ac.recruit.gateways.ProductGateway;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class GetProductsChildrenTest {

    @InjectMocks
    private GetProductChildren getProductChildren;

    @Mock
    private ProductGateway productGateway;

    @Test
    public void testGetProductChildren(){
        getProductChildren.execute(1);
        verify(productGateway).findProductChildren(anyInt());
    }
}
