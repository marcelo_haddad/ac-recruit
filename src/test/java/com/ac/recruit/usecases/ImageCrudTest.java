package com.ac.recruit.usecases;

import com.ac.recruit.domains.Image;
import com.ac.recruit.domains.Product;
import com.ac.recruit.domains.exceptions.ResourceNotFoundException;
import com.ac.recruit.gateways.ImageGateway;
import com.ac.recruit.gateways.ProductGateway;
import com.ac.recruit.gateways.http.jsons.ImageRequest;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ImageCrudTest {

    @InjectMocks
    private ImageCrud imageCrud;

    @Mock
    private ImageGateway imageGateway;

    @Mock
    private ProductGateway productGateway;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testSave() {
        imageCrud.executeSave(new ImageRequest());

        verify(imageGateway).save(any(Image.class));
    }

    @Test
    public void testDelete() {
        imageCrud.executeDelete(1);

        verify(imageGateway).deleteById(anyInt());
    }

    @Test
    public void testUpdate() {
        when(productGateway.findById(anyInt())).thenReturn(new Product());
        when(imageGateway.findById(anyInt())).thenReturn(new Image());

        imageCrud.executeUpdate(new ImageRequest(), 1);

        verify(imageGateway).findById(anyInt());
        verify(productGateway).findById(anyInt());
        verify(imageGateway).save(any(Image.class));
    }

    @Test
    public void testUpdateWithInvalidId() {
        expectedException.expect(ResourceNotFoundException.class);
        expectedException.expectMessage("Image not found with id 1");

        imageCrud.executeUpdate(new ImageRequest(), 1);

        verify(imageGateway).findById(anyInt());
        verify(productGateway).findById(anyInt());
        verify(imageGateway).save(any(Image.class));
    }

    @Test
    public void testUpdateWithInvalidProductId() {
        expectedException.expect(ResourceNotFoundException.class);
        expectedException.expectMessage("Product not found with id 2");

        when(imageGateway.findById(anyInt())).thenReturn(new Image());

        ImageRequest request = new ImageRequest();
        request.setProductId(2);
        imageCrud.executeUpdate(request, 1);

        verify(imageGateway).findById(anyInt());
        verify(productGateway).findById(anyInt());
        verify(imageGateway).save(any(Image.class));
    }
}
