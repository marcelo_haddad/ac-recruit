package com.ac.recruit.usecases;

import com.ac.recruit.domains.Product;
import com.ac.recruit.gateways.ImageGateway;
import com.ac.recruit.gateways.ProductGateway;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GetProductByIdTest {

    @InjectMocks
    private GetProductById getProductById;

    @Mock
    private ProductGateway productGateway;

    @Mock
    private ImageGateway imageGateway;

    @Test
    public void testFindWithoutRelationships() {
        when(productGateway.findByIdWithoutChildren(anyInt())).thenReturn(new Product());

        getProductById.execute(1, false, false);

        verify(productGateway, times(0)).findById(anyInt());
        verify(imageGateway, times(0)).findByProductId(anyInt());
        verify(productGateway).findByIdWithoutChildren(anyInt());
    }

    @Test
    public void testFindOnlyWithChildren() {
        when(productGateway.findById(anyInt())).thenReturn(new Product());

        getProductById.execute(1, false, true);

        verify(productGateway, times(0)).findByIdWithoutChildren(anyInt());
        verify(imageGateway, times(0)).findByProductId(anyInt());
        verify(productGateway).findById(anyInt());
    }

    @Test
    public void testFindWithChildrenAndImages() {
        when(productGateway.findById(anyInt())).thenReturn(new Product());
        when(imageGateway.findByProductId(anyInt())).thenReturn(new ArrayList<>());

        getProductById.execute(1, true, true);

        verify(productGateway, times(0)).findByIdWithoutChildren(anyInt());
        verify(imageGateway).findByProductId(anyInt());
        verify(productGateway).findById(anyInt());
    }
}
