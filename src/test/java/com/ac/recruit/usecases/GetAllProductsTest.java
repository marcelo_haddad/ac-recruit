package com.ac.recruit.usecases;

import com.ac.recruit.domains.Image;
import com.ac.recruit.domains.Product;
import com.ac.recruit.gateways.ImageGateway;
import com.ac.recruit.gateways.ProductGateway;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GetAllProductsTest {

    @InjectMocks
    private GetAllProducts getAllProducts;

    @Mock
    private ProductGateway productGateway;

    @Mock
    private ImageGateway imageGateway;

    @Test
    public void testFindAllWithoutRelationship(){
        getAllProducts.execute(false, false);

        verify(productGateway, times(0)).findAll();
        verify(imageGateway, times(0)).findByProductsIds(Mockito.anyList());
        verify(productGateway).findAllWithoutChildren();
    }

    @Test
    public void testFindAllOnlyWithChildren(){
        getAllProducts.execute(false, true);

        verify(productGateway, times(0)).findAllWithoutChildren();
        verify(imageGateway, times(0)).findByProductsIds(Mockito.anyList());
        verify(productGateway).findAll();
    }

    @Test
    public void testFindAllWithChildrenAndImages(){
        Product product = new Product();
        product.setId(1);

        Product product2 = new Product();
        product.setId(2);

        Image image = new Image();
        image.setProduct(product);

        Image image2 = new Image();
        image.setProduct(product2);

        when(productGateway.findAll()).thenReturn(Arrays.asList(product));
        when(imageGateway.findByProductId(anyInt())).thenReturn(Arrays.asList(image, image2));
        List<Product> products = getAllProducts.execute(true, true);

        verify(productGateway, times(0)).findAllWithoutChildren();
        verify(imageGateway).findByProductsIds(Mockito.anyList());
        verify(productGateway).findAll();

        Assert.assertThat(products.size(), Matchers.equalTo(1));
        Assert.assertThat(products, Matchers.contains(product));
    }
}
