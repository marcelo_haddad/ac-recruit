package com.ac.recruit.usecases;

import com.ac.recruit.domains.Product;
import com.ac.recruit.domains.exceptions.ResourceNotFoundException;
import com.ac.recruit.gateways.ProductGateway;
import com.ac.recruit.gateways.http.jsons.ProductRequest;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ProductCrudTest {

    @InjectMocks
    private ProductCrud productCrud;

    @Mock
    private ProductGateway productGateway;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testSave() {
        productCrud.executeSave(new ProductRequest());

        verify(productGateway, times(0)).findById(anyInt());
        verify(productGateway).save(any(Product.class));
    }

    @Test
    public void testSaveWithParentProduct() {
        ProductRequest request = new ProductRequest();
        request.setParentProductId(2);
        productCrud.executeSave(request);

        verify(productGateway).findById(anyInt());
        verify(productGateway).save(any(Product.class));
    }

    @Test
    public void testUpdate() throws ResourceNotFoundException {
        when(productGateway.findById(anyInt())).thenReturn(new Product());

        productCrud.executeUpdate(new ProductRequest(), 1);

        verify(productGateway, times(1)).findById(anyInt());
        verify(productGateway).save(any(Product.class));
    }

    @Test
    public void testUpdateWithParentProduct() throws ResourceNotFoundException {
        when(productGateway.findById(anyInt())).thenReturn(new Product());

        ProductRequest request = new ProductRequest();
        request.setParentProductId(2);
        productCrud.executeUpdate(request, 2);

        verify(productGateway, times(2)).findById(anyInt());
        verify(productGateway).save(any(Product.class));
    }

    @Test
    public void testUpdateWithInvalidProductId() throws ResourceNotFoundException {
        expectedException.expect(ResourceNotFoundException.class);
        expectedException.expectMessage("Product not found with id 2");

        ProductRequest request = new ProductRequest();
        request.setParentProductId(1);
        productCrud.executeUpdate(request, 2);

        verify(productGateway, times(1)).findById(anyInt());
        verify(productGateway).save(any(Product.class));
    }

    @Test
    public void testUpdateWithInvalidParentProductId() throws ResourceNotFoundException {
        expectedException.expect(ResourceNotFoundException.class);
        expectedException.expectMessage("Parent product not found with id 1");

        when(productGateway.findById(33)).thenReturn(new Product());

        ProductRequest request = new ProductRequest();
        request.setParentProductId(1);
        productCrud.executeUpdate(request, 33);

        verify(productGateway, times(1)).findById(33);
        verify(productGateway).save(any(Product.class));
    }

    @Test
    public void testDelete() {
        productCrud.executeDelete(1);

        verify(productGateway).deleteById(anyInt());
    }
}
