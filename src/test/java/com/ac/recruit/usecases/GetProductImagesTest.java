package com.ac.recruit.usecases;

import com.ac.recruit.gateways.ImageGateway;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class GetProductImagesTest {

    @InjectMocks
    private GetProductImages getProductImages;

    @Mock
    private ImageGateway imageGateway;

    @Test
    public void testGetProductImages(){
        getProductImages.execute(1);

        verify(imageGateway).findByProductId(1);
    }
}
