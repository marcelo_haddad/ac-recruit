package com.ac.recruit.gateways.h2;

import com.ac.recruit.domains.Product;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@DataJpaTest
public class ProductGatewayImplTest {
    @Autowired
    private ProductRepository repository;

    private ProductGatewayImpl gateway;

    @Before
    public void setUp() {
        gateway = new ProductGatewayImpl(repository);
        repository.deleteAll();
    }

    @Test
    public void testSave() {
        Product product = new Product();
        product.setName("biscuit");
        product.setDescription("biscuit");

        Product productSaved = gateway.save(product);
        Product productFound = gateway.findById(productSaved.getId());
        assertThat(productSaved.getName(), equalTo(productFound.getName()));
        assertThat(productSaved.getDescription(), equalTo(productFound.getDescription()));
    }

    @Test
    public void testSaveWithParentProduct() {
        Product parentProduct = new Product();
        parentProduct.setName("biscuit");
        parentProduct.setDescription("biscuit");

        Product parentProductSaved = gateway.save(parentProduct);

        Product childProduct = new Product();
        childProduct.setName("biscuit");
        childProduct.setDescription("biscuit");
        childProduct.setParentProduct(parentProductSaved);

        Product childProductSaved = gateway.save(childProduct);

        Product productFound = gateway.findById(childProductSaved.getId());
        assertThat(childProductSaved.getName(), equalTo(productFound.getName()));
        assertThat(childProductSaved.getDescription(), equalTo(productFound.getDescription()));
        assertThat(childProductSaved.getParentProduct().getId(), equalTo(productFound.getParentProduct().getId()));
    }

    @Test
    public void testFindAll() {
        Product parentProduct = new Product();
        parentProduct.setName("biscuit");
        parentProduct.setDescription("biscuit");

        Product parentProductSaved = gateway.save(parentProduct);

        Product childProduct = new Product();
        childProduct.setName("biscuit");
        childProduct.setDescription("biscuit");
        childProduct.setParentProduct(parentProductSaved);

        gateway.save(childProduct);

        List<Product> allProducts = gateway.findAll();
        assertThat(allProducts.size(), equalTo(2));
    }

    @Test
    public void testFindAllWithoutChildren() {
        Product parentProduct = new Product();
        parentProduct.setName("biscuit");
        parentProduct.setDescription("biscuit");

        Product parentProductSaved = gateway.save(parentProduct);

        Product childProduct = new Product();
        childProduct.setName("biscuit");
        childProduct.setDescription("biscuit");
        childProduct.setParentProduct(parentProductSaved);

        gateway.save(childProduct);

        List<Product> allProducts = gateway.findAllWithoutChildren();
        assertThat(allProducts.size(), equalTo(1));
    }

    @Test
    public void testFindProductChildren() {
        Product parentProduct = new Product();
        parentProduct.setName("biscuit");
        parentProduct.setDescription("biscuit");

        Product parentProductSaved = gateway.save(parentProduct);

        Product childProduct = new Product();
        childProduct.setName("biscuit");
        childProduct.setDescription("biscuit");
        childProduct.setParentProduct(parentProductSaved);

        gateway.save(childProduct);

        List<Product> allProducts = gateway.findProductChildren(parentProductSaved.getId());
        assertThat(allProducts.size(), equalTo(1));
        assertThat(allProducts, contains(childProduct));
    }

    @Test
    public void testFindByIdWithoutChildren() {
        Product parentProduct = new Product();
        parentProduct.setName("biscuit");
        parentProduct.setDescription("biscuit");

        Product parentProductSaved = gateway.save(parentProduct);

        Product childProduct = new Product();
        childProduct.setName("biscuit");
        childProduct.setDescription("biscuit");
        childProduct.setParentProduct(parentProductSaved);

        gateway.save(childProduct);

        Product parentProductFound = gateway.findByIdWithoutChildren(parentProduct.getId());
        assertThat(parentProductFound, notNullValue());

        Product childProductFound = gateway.findByIdWithoutChildren(childProduct.getId());
        assertThat(childProductFound, nullValue());
    }

    @Test
    public void testDeleteById() {
        Product parentProduct = new Product();
        parentProduct.setName("biscuit");
        parentProduct.setDescription("biscuit");

        Product parentProductSaved = gateway.save(parentProduct);

        gateway.deleteById(parentProductSaved.getId());

        List<Product> allProducts = gateway.findAll();
        assertThat(allProducts, empty());
    }
}
