package com.ac.recruit.gateways.h2;


import com.ac.recruit.domains.Image;
import com.ac.recruit.domains.Product;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@DataJpaTest
public class ImageGatewayImplTest {

    @Autowired
    private ImageRepository repository;

    @Autowired
    private ProductRepository productRepository;

    private ImageGatewayImpl gateway;

    @Before
    public void setUp() {
        gateway = new ImageGatewayImpl(repository);
        repository.deleteAll();
        productRepository.deleteAll();
    }

    @Test
    public void testSave() {
        Product product = new Product();
        product.setName("biscuit");
        product.setDescription("biscuit");
        productRepository.save(product);

        Image image = new Image();
        image.setType("image");
        image.setProduct(product);

        Image imageSaved = gateway.save(image);
        Image imageFound = gateway.findById(imageSaved.getId());
        assertThat(imageSaved.getType(), equalTo(imageFound.getType()));
        assertThat(imageSaved.getProduct(), equalTo(imageFound.getProduct()));
    }

    @Test
    public void testDeleteById() {
        Product product = new Product();
        product.setName("biscuit");
        product.setDescription("biscuit");
        productRepository.save(product);

        Image image = new Image();
        image.setType("image");
        image.setProduct(product);

        Image imageSaved = gateway.save(image);
        gateway.deleteById(imageSaved.getId());
        List<Image> allImages = gateway.findAll();
        assertThat(allImages.size(), equalTo(0));
    }

    @Test
    public void testFindByProductId() {
        Product product = new Product();
        product.setName("biscuit");
        product.setDescription("biscuit");
        Product productSaved = productRepository.save(product);

        Product product2 = new Product();
        product2.setName("biscuit");
        product2.setDescription("biscuit");
        Product productSaved2 = productRepository.save(product2);

        Image image = new Image();
        image.setType("image");
        image.setProduct(productSaved);

        Image image2 = new Image();
        image2.setType("image2");
        image2.setProduct(productSaved2);

        gateway.save(image);
        gateway.save(image2);
        List<Image> productImages = gateway.findByProductId(productSaved.getId());

        assertThat(productImages.size(), equalTo(1));
    }

    @Test
    public void testFindByProductsIds() {
        Product product = new Product();
        product.setName("biscuit");
        product.setDescription("biscuit");
        Product productSaved = productRepository.save(product);

        Product product2 = new Product();
        product2.setName("biscuit");
        product2.setDescription("biscuit");
        Product productSaved2 = productRepository.save(product2);

        Image image = new Image();
        image.setType("image");
        image.setProduct(productSaved);

        Image image2 = new Image();
        image2.setType("image2");
        image2.setProduct(productSaved2);

        gateway.save(image);
        gateway.save(image2);
        List<Image> productImages = gateway.findByProductsIds(Arrays.asList(productSaved.getId(), productSaved2.getId()));

        assertThat(productImages.size(), equalTo(2));
    }
}
