package com.ac.recruit.conf;

import com.ac.recruit.gateways.http.ImageResource;
import com.ac.recruit.gateways.http.ProductResource;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        registerEndpoints();
    }

    private void registerEndpoints() {
        register(ProductResource.class);
        register(ImageResource.class);
    }
}
