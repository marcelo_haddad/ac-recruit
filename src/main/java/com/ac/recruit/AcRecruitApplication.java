package com.ac.recruit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AcRecruitApplication {

	public static void main(String[] args) {
		SpringApplication.run(AcRecruitApplication.class, args);
	}
}
