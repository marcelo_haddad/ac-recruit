package com.ac.recruit.usecases;

import com.ac.recruit.domains.Image;
import com.ac.recruit.domains.Product;
import com.ac.recruit.domains.exceptions.ResourceNotFoundException;
import com.ac.recruit.gateways.ImageGateway;
import com.ac.recruit.gateways.ProductGateway;
import com.ac.recruit.gateways.http.jsons.ImageRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ImageCrud {

    private ImageGateway imageGateway;
    private ProductGateway productGateway;

    @Autowired
    public ImageCrud(ImageGateway imageGateway, ProductGateway productGateway) {
        this.imageGateway = imageGateway;
        this.productGateway = productGateway;
    }

    public Image executeSave(ImageRequest request) {
        Image image = new Image();
        image.setType(request.getType());
        Product product = productGateway.findById(request.getProductId());
        image.setProduct(product);

        return imageGateway.save(image);
    }

    public void executeUpdate(ImageRequest request, Integer id) {
        Image image = Optional.ofNullable(imageGateway.findById(id))
                .orElseThrow(() -> new ResourceNotFoundException("Image not found with id " + id));

        Product product =
                Optional.ofNullable(productGateway.findById(request.getProductId()))
                        .orElseThrow(() -> new ResourceNotFoundException("Product not found with id " + request.getProductId()));

        image.setType(request.getType());
        image.setProduct(product);
        imageGateway.save(image);
    }

    public void executeDelete(Integer id) {
        imageGateway.deleteById(id);
    }


}
