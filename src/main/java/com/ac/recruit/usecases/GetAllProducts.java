package com.ac.recruit.usecases;

import com.ac.recruit.domains.Image;
import com.ac.recruit.domains.Product;
import com.ac.recruit.gateways.ImageGateway;
import com.ac.recruit.gateways.ProductGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class GetAllProducts {

    private ProductGateway gateway;
    private ImageGateway imageGateway;

    @Autowired
    public GetAllProducts(ProductGateway gateway, ImageGateway imageGateway) {
        this.gateway = gateway;
        this.imageGateway = imageGateway;
    }

    public List<Product> execute(Boolean loadImages, Boolean loadChildren) {
        List<Product> products = loadChildren ? gateway.findAll() : gateway.findAllWithoutChildren();

        if (loadImages) {
            List<Image> productsImages = getProductsImages(products);
            products.forEach(product -> {
                        List<Image> images = productsImages.stream().filter(image -> product.getId().equals(image.getProduct().getId())).collect(Collectors.toList());
                        product.setImages(images);
                    }
            );
        }

        return products;
    }

    private List<Image> getProductsImages(List<Product> products) {
        List<Integer> productsId = products.stream().map(Product::getId).collect(Collectors.toList());
        return imageGateway.findByProductsIds(productsId);
    }

}
