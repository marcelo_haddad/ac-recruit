package com.ac.recruit.usecases;

import com.ac.recruit.domains.Product;
import com.ac.recruit.gateways.ProductGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

@Component
public class GetProductChildren {

    private ProductGateway productGateway;

    @Autowired
    public GetProductChildren(ProductGateway productGateway) {
        this.productGateway = productGateway;
    }

    public List<Product> execute(Integer parentProductId) {
        return productGateway.findProductChildren(parentProductId);
    }
}
