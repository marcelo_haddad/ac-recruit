package com.ac.recruit.usecases;

import com.ac.recruit.domains.Product;
import com.ac.recruit.domains.exceptions.ResourceNotFoundException;
import com.ac.recruit.gateways.ProductGateway;
import com.ac.recruit.gateways.http.jsons.ProductRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ProductCrud {

    private ProductGateway productGateway;

    @Autowired
    public ProductCrud(ProductGateway productGateway) {
        this.productGateway = productGateway;
    }

    public Product executeSave(ProductRequest request) {
        Product product = new Product();
        product.setName(request.getName());
        product.setDescription(request.getDescription());
        if (request.getParentProductId() != null) {
            Product parentProduct = productGateway.findById(request.getParentProductId());
            product.setParentProduct(parentProduct);
        }

        return productGateway.save(product);
    }

    public void executeUpdate(ProductRequest request, Integer id) {
        Product product = Optional.ofNullable(productGateway.findById(id))
                .orElseThrow(() -> new ResourceNotFoundException("Product not found with id " + id));

        product.setName(request.getName());
        product.setDescription(request.getDescription());
        if (request.getParentProductId() != null) {

            Product parentProduct =
                    Optional.ofNullable(productGateway.findById(request.getParentProductId()))
                            .orElseThrow(() -> new ResourceNotFoundException("Parent product not found with id " + request.getParentProductId()));

            product.setParentProduct(parentProduct);
        }
        productGateway.save(product);
    }

    public void executeDelete(Integer id) {
        productGateway.deleteById(id);
    }

}
