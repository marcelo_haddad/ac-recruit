package com.ac.recruit.usecases;

import com.ac.recruit.domains.Image;
import com.ac.recruit.domains.Product;
import com.ac.recruit.gateways.ImageGateway;
import com.ac.recruit.gateways.ProductGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GetProductById {

    private ProductGateway productGateway;
    private ImageGateway imageGateway;

    @Autowired
    public GetProductById(ProductGateway productGateway, ImageGateway imageGateway) {
        this.productGateway = productGateway;
        this.imageGateway = imageGateway;
    }

    public Product execute(Integer id, Boolean loadImages, Boolean loadChildren) {
        Product product = loadChildren ? productGateway.findById(id) : productGateway.findByIdWithoutChildren(id);

        if (loadImages) {
            List<Image> productImages = getProductImages(product.getId());
            product.setImages(productImages);
        }

        return product;
    }

    private List<Image> getProductImages(Integer id) {
        return imageGateway.findByProductId(id);
    }
}
