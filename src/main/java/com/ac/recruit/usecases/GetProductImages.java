package com.ac.recruit.usecases;

import com.ac.recruit.domains.Image;
import com.ac.recruit.gateways.ImageGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GetProductImages {

    private ImageGateway imageGateway;

    @Autowired
    public GetProductImages(ImageGateway imageGateway) {
        this.imageGateway = imageGateway;
    }

    public List<Image> execute(Integer productId) {
        return imageGateway.findByProductId(productId);
    }
}
