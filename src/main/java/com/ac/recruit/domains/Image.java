package com.ac.recruit.domains;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;


@Getter
@Setter
@Entity
@Table(name = "IMAGE",
        indexes = {
                @Index(name = "idx_image_id", columnList = "id"),
                @Index(name = "idx_image_product", columnList = "PRODUCT_ID")
        }
)
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Image {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "TYPE", nullable = false)
    private String type;

    @ManyToOne
    @JoinColumn(name = "PRODUCT_ID")
    @JsonIgnore
    private Product product;

    public Image(Integer id, String type) {
        this.id = id;
        this.type = type;
    }
}
