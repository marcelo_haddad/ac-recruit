package com.ac.recruit.gateways;

import com.ac.recruit.domains.Image;

import java.util.List;

public interface ImageGateway {

    Image save(Image image);

    void deleteById(Integer id);

    Image findById(Integer id);

    List<Image> findAll();

    List<Image> findByProductId(Integer productId);

    List<Image> findByProductsIds(List<Integer> productsIds);
}
