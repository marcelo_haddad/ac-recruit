package com.ac.recruit.gateways.http;

import com.ac.recruit.domains.Image;
import com.ac.recruit.gateways.http.jsons.ImageRequest;
import com.ac.recruit.usecases.GetProductImages;
import com.ac.recruit.usecases.ImageCrud;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

@Component
@Path("/images")
public class ImageResource {

    private ImageCrud imageCrud;
    private GetProductImages getProductImages;

    @Autowired
    public ImageResource(ImageCrud imageCrud, GetProductImages getProductImages) {
        this.imageCrud = imageCrud;
        this.getProductImages = getProductImages;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveImage(ImageRequest request) {

        Image imageSaved = imageCrud.executeSave(request);
        return Response.created(URI.create("/images" + imageSaved.getId())).build();
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response deleteImage(@PathParam("id") int id) {
        imageCrud.executeDelete(id);
        return Response.noContent().build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response updateImage(ImageRequest request, @PathParam("id") int id) {
        imageCrud.executeUpdate(request, id);
        return Response.accepted().build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProductImages(ImageRequest request, @QueryParam("productId") int productId) {
        return Response.ok(getProductImages.execute(productId)).build();
    }
}
