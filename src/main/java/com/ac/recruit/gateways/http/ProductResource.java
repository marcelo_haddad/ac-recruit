package com.ac.recruit.gateways.http;

import com.ac.recruit.domains.Product;
import com.ac.recruit.domains.exceptions.ResourceNotFoundException;
import com.ac.recruit.gateways.http.jsons.ProductRequest;
import com.ac.recruit.usecases.GetAllProducts;
import com.ac.recruit.usecases.GetProductById;
import com.ac.recruit.usecases.GetProductChildren;
import com.ac.recruit.usecases.ProductCrud;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

@Component
@Path("/products")
public class ProductResource {

    private ProductCrud productCrud;
    private GetAllProducts getAllProducts;
    private GetProductById getProductById;
    private GetProductChildren getProductChildren;

    @Autowired
    public ProductResource(ProductCrud productCrud, GetAllProducts getAllProducts, GetProductById getProductById, GetProductChildren getProductChildren) {
        this.productCrud = productCrud;
        this.getAllProducts = getAllProducts;
        this.getProductById = getProductById;
        this.getProductChildren = getProductChildren;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveProduct(ProductRequest request) {

        Product productSaved = productCrud.executeSave(request);
        return Response.created(URI.create("/product" + productSaved.getId())).build();
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response deleteProduct(@PathParam("id") int id) {
        productCrud.executeDelete(id);
        return Response.noContent().build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response updateProduct(ProductRequest request, @PathParam("id") int id) throws ResourceNotFoundException{
        productCrud.executeUpdate(request, id);
        return Response.accepted().build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllProducts(@QueryParam("loadImages") boolean loadImages,
                                   @QueryParam("loadChildren") boolean loadChildren) {
        return Response.ok(getAllProducts.execute(loadImages, loadChildren)).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProduct(@PathParam("id") int id,
                               @QueryParam("loadImages") boolean loadImages,
                               @QueryParam("loadChildren") boolean loadChildren) {

        return Response.ok(getProductById.execute(id, loadImages, loadChildren)).build();
    }

    @GET
    @Path("/children")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProductChildren(@QueryParam("parentProductId") int parentProductId) {
        return Response.ok(getProductChildren.execute(parentProductId)).build();
    }
}
