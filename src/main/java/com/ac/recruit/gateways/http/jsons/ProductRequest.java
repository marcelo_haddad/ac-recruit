package com.ac.recruit.gateways.http.jsons;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class ProductRequest implements Serializable {

    @NotNull
    private String name;

    @NotNull
    private String description;

    private Integer parentProductId;
}
