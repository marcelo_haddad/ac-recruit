package com.ac.recruit.gateways.h2;

import com.ac.recruit.domains.Image;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ImageRepository extends CrudRepository<Image, Integer> {

    @Query("SELECT NEW Image(id, type) FROM Image WHERE product.id = ? ")
    List<Image> findByProductId(Integer product);

    List<Image> findByProductIdIn(List<Integer> product);
}
