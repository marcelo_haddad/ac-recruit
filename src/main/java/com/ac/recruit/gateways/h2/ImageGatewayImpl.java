package com.ac.recruit.gateways.h2;

import com.ac.recruit.domains.Image;
import com.ac.recruit.gateways.ImageGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ImageGatewayImpl implements ImageGateway {

    private ImageRepository imageRepository;

    @Autowired
    public ImageGatewayImpl(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    @Override
    public Image save(Image image) {
        return imageRepository.save(image);
    }

    @Override
    public void deleteById(Integer id) {
        imageRepository.delete(id);
    }

    @Override
    public Image findById(Integer id) {
        return imageRepository.findOne(id);
    }

    @Override
    public List<Image> findAll() {
        return (List<Image>) imageRepository.findAll();
    }

    @Override
    public List<Image> findByProductId(Integer productId) {
        return imageRepository.findByProductId(productId);
    }

    @Override
    public List<Image> findByProductsIds(List<Integer> productsIds) {
        return imageRepository.findByProductIdIn(productsIds);
    }
}
