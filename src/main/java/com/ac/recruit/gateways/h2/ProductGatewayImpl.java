package com.ac.recruit.gateways.h2;

import com.ac.recruit.domains.Product;
import com.ac.recruit.gateways.ProductGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

@Component
public class ProductGatewayImpl implements ProductGateway {

    private ProductRepository repository;

    @Autowired
    public ProductGatewayImpl(ProductRepository repository) {
        this.repository = repository;
    }

    @Override
    public Product findById(Integer id) {
        return repository.findOne(id);
    }

    @Override
    public List<Product> findAll() {
        return (List<Product>) repository.findAll();
    }

    @Override
    public List<Product> findProductChildren(Integer id) {
        return repository.findByParentProductId(id);
    }

    @Override
    public List<Product> findAllWithoutChildren() {
        return repository.findByParentProductIsNull();
    }

    @Override
    public Product findByIdWithoutChildren(Integer id) {
        return repository.findByIdAndParentProductIsNull(id);
    }

    @Override
    public Product save(Product product) {
        return repository.save(product);
    }

    @Override
    public void deleteById(Integer id) {
        repository.delete(id);
    }


}
