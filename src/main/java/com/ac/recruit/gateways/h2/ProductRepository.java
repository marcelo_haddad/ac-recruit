package com.ac.recruit.gateways.h2;

import com.ac.recruit.domains.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

public interface ProductRepository extends CrudRepository<Product, Integer> {

    List<Product> findByParentProductIsNull();

    Product findByIdAndParentProductIsNull(Integer id);

    List<Product> findByParentProductId(Integer id);
}
