package com.ac.recruit.gateways;

import com.ac.recruit.domains.Product;

import java.util.List;
import java.util.Set;

public interface ProductGateway {

    Product findById(Integer id);

    List<Product> findAll();

    List<Product> findProductChildren(Integer id);

    List<Product> findAllWithoutChildren();

    Product findByIdWithoutChildren(Integer id);

    Product save(Product product);

    void deleteById(Integer id);
}
